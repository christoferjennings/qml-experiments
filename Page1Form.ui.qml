import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Page {
    title: qsTr("Page 1: What happened?")


    /* This ScrollView doesn't do what I think it should do.
     * I want it to scroll the whole page but on this page it only does the text fields.
     */
    ScrollView {
        id: scroll_view
        clip: true
        width: parent.width
        height: parent.height

        ColumnLayout {
            id: the_column
            spacing: 20
            width: Math.max(scroll_view.width * 0.8, 300)
            implicitWidth: width
            implicitHeight: Label.height + label2.height + btn.height + (2 * spacing)

            anchors.top: parent.top
            anchors.topMargin: 64
            anchors.horizontalCenter: parent.horizontalCenter

            FancyTextField {
                id: label1
                label: qsTr("This")
                placeholderText: qsTr("type something")
                Layout.fillWidth: true
            }
            FancyTextField {
                id: label2
                label: qsTr("That")
                placeholderText: qsTr("type something else")
                Layout.fillWidth: true
            }
            Button {
                id: btn
                text: "doit"
                Layout.alignment: Qt.AlignCenter
            }
        }

        /* This dumb Rectangle makes the layout center correctly. Why? */
        //Rectangle {}
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

