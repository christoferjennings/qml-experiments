
I'm trying to figure out QML layouts. This project is based on the QML Stack application. The home page and page 1 code are identical except for a comment and an empty rectangle. Some issue I'm trying to understand...

- Home page has a centered layout but it only works if there is an extra element outside the ColumnLayout (e.g., a Rectangle). How can I get the centered layout with just the ColumnLayout?

- Page 1 removes the extra rectangle but the layout is not centered. Also the layout changes when I resize the window. It's really just meant to compare to the Home page.

- ScrollView ... I don't get it. I want it to scroll the whole page. Home page doesn't scroll at all. Page 1 will scroll but no enough to show all the things in the ColumnLayout (the button doesn't show up).

## Screen shots

### Layout

Home page showing the layout I want, but only because of the hidden Rectangle.

![Home page](screenshot-home.png)

Page1 showing the layout without the hidden Rectangle.

![Page1](screenshot-page1.png)

### Scrolling

Home page with window shrunk but no scrolling available.

![Home page showing no scrolling](screenshot-home-no-scroll.png)

Page 1 with window shrunk showing partial scrolling available. (Note the scrollbars.)

![Page1 showing partial scrolling](screenshot-page1-partial-scroll.png)
