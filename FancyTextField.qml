import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    id: frame
    color: "#f0f0f0"
    border.color: "#0909f0"
    border.width: 1

    implicitHeight: body.implicitHeight

    property alias label: label.text
    property alias text: field.text
    property alias placeholderText: field.placeholderText

    Rectangle {
        id: body
        color: "#00000000"
        anchors.fill: parent
        implicitHeight: label.height + field.height + 12
    }

    Label {
        id: label
        text: qsTr("text")
        anchors.top: parent.top
        anchors.topMargin: 6
        anchors.left: parent.left
        anchors.leftMargin: 10
    }

    TextField {
        id: field
        anchors.top: label.bottom
        anchors.left: label.left
        anchors.leftMargin: -10
        anchors.right: parent.right
        background: Rectangle {
            color: "#00000000"
        }
    }
}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
